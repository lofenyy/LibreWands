//     This file is part of LibreWands.

//    LibreWands is free software: you can redistribute it and/or modify it under the terms of the
//    GNU General Public License as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.

//    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//    the GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License along with LibreWands. If
//   not, see https://www.gnu.org/licenses/.

// ---

#include "engineMaps.h"
#include <ncurses.h>
#include <string.h>

// To run at the beginning of the game.
void initEngine()
	{
	// Set parameters to standard values.
	initscr();
	start_color();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);
	halfdelay(2);
	cbreak();

	// Individually initialize all colours. (Plz someone fix me!)
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_YELLOW, COLOR_BLACK);
	init_pair(5, COLOR_BLUE, COLOR_BLACK);
	init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(7, COLOR_CYAN, COLOR_BLACK);
	init_pair(8, COLOR_WHITE, COLOR_BLACK);

	init_pair(11, COLOR_BLACK, COLOR_RED);
	init_pair(12, COLOR_RED, COLOR_RED);
	init_pair(13, COLOR_GREEN, COLOR_RED);
	init_pair(14, COLOR_YELLOW, COLOR_RED);
	init_pair(15, COLOR_BLUE, COLOR_RED);
	init_pair(16, COLOR_MAGENTA, COLOR_RED);
	init_pair(17, COLOR_CYAN, COLOR_RED);
	init_pair(18, COLOR_WHITE, COLOR_RED);

	init_pair(21, COLOR_BLACK, COLOR_GREEN);
	init_pair(22, COLOR_RED, COLOR_GREEN);
	init_pair(23, COLOR_GREEN, COLOR_GREEN);
	init_pair(24, COLOR_YELLOW, COLOR_GREEN);
	init_pair(25, COLOR_BLUE, COLOR_GREEN);
	init_pair(26, COLOR_MAGENTA, COLOR_GREEN);
	init_pair(27, COLOR_CYAN, COLOR_GREEN);
	init_pair(28, COLOR_WHITE, COLOR_GREEN);

	init_pair(31, COLOR_BLACK, COLOR_YELLOW);
	init_pair(32, COLOR_RED, COLOR_YELLOW);
	init_pair(33, COLOR_GREEN, COLOR_YELLOW);
	init_pair(34, COLOR_YELLOW, COLOR_YELLOW);
	init_pair(35, COLOR_BLUE, COLOR_YELLOW);
	init_pair(36, COLOR_MAGENTA, COLOR_YELLOW);
	init_pair(37, COLOR_CYAN, COLOR_YELLOW);
	init_pair(38, COLOR_WHITE, COLOR_YELLOW);

	init_pair(41, COLOR_BLACK, COLOR_BLUE);
	init_pair(42, COLOR_RED, COLOR_BLUE);
	init_pair(43, COLOR_GREEN, COLOR_BLUE);
	init_pair(44, COLOR_YELLOW, COLOR_BLUE);
	init_pair(45, COLOR_BLUE, COLOR_BLUE);
	init_pair(46, COLOR_MAGENTA, COLOR_BLUE);
	init_pair(47, COLOR_CYAN, COLOR_BLUE);
	init_pair(48, COLOR_WHITE, COLOR_BLUE);

	init_pair(51, COLOR_BLACK, COLOR_MAGENTA);
	init_pair(52, COLOR_RED, COLOR_MAGENTA);
	init_pair(53, COLOR_GREEN, COLOR_MAGENTA);
	init_pair(54, COLOR_YELLOW, COLOR_MAGENTA);
	init_pair(55, COLOR_BLUE, COLOR_MAGENTA);
	init_pair(56, COLOR_MAGENTA, COLOR_MAGENTA);
	init_pair(57, COLOR_CYAN, COLOR_MAGENTA);
	init_pair(58, COLOR_WHITE, COLOR_MAGENTA);

	init_pair(61, COLOR_BLACK, COLOR_CYAN);
	init_pair(62, COLOR_RED, COLOR_CYAN);
	init_pair(63, COLOR_GREEN, COLOR_CYAN);
	init_pair(64, COLOR_YELLOW, COLOR_CYAN);
	init_pair(65, COLOR_BLUE, COLOR_CYAN);
	init_pair(66, COLOR_MAGENTA, COLOR_CYAN);
	init_pair(67, COLOR_CYAN, COLOR_CYAN);
	init_pair(68, COLOR_WHITE, COLOR_CYAN);

	init_pair(71, COLOR_BLACK, COLOR_WHITE);
	init_pair(72, COLOR_RED, COLOR_WHITE);
	init_pair(73, COLOR_GREEN, COLOR_WHITE);
	init_pair(74, COLOR_YELLOW, COLOR_WHITE);
	init_pair(75, COLOR_BLUE, COLOR_WHITE);
	init_pair(76, COLOR_MAGENTA, COLOR_WHITE);
	init_pair(77, COLOR_CYAN, COLOR_WHITE);
	init_pair(78, COLOR_WHITE, COLOR_WHITE);
	}

// Get user input from a number of devices. Internally emulate Gameboy controls.
int joycon()
	{
	int k = 0;
	k = getch();

	switch (k)
		{
		// Up button on d-pad
		case KEY_UP:
		case 'w':
		case 'i':
		case 'W':
		case 'I':
			return 'w';
			break;

		// Down button on d-pad
		case KEY_DOWN:
		case 's':
		case 'k':
		case 'S':
		case 'K':
			return 's';
			break;

		// Left button on d-pad
		case KEY_LEFT:
		case 'a':
		case 'j':
		case 'A':
		case 'J':
			return 'a';
			break;

		// Right button on d-pad
		case KEY_RIGHT:
		case 'd':
		case 'l':
		case 'D':
		case 'L':
			return 'd';
			break;

		// Start button
		case '\n':
			return '\n';
			break;

		// Select button
		case '\\':
			return '\\';
			break;

		// A button
		case 'z':
		case 'Z':
			return 'z';
			break;

		// B button
		case 'x':
		case 'X':
			return 'x';
			break;
		}

	// If no key is pressed, return 0.
	return 0;
	}
