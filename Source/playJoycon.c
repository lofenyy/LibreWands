//     This file is part of LibreWands.

//    LibreWands is free software: you can redistribute it and/or modify it under the terms of the
//    GNU General Public License as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.

//    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//    the GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License along with LibreWands. If
//   not, see https://www.gnu.org/licenses/.

#include "engineCore.c"

// Get controller input and display which button was last pressed.
int main()
	{
	// Start our game engine
	initEngine();

	// Store our cursor position
	int cursorPos = 0;

	// While getting controller input
	for (int k = 0;; k = joycon())
		{
		// Assign a value to which line the cursor will appear on.
		switch (k)
			{
			case 'w':
				cursorPos = 0;
				break;
			case 's':
				cursorPos = 1;
				break;
			case 'a':
				cursorPos = 2;
				break;
			case 'd':
				cursorPos = 3;
				break;
			case 'z':
				cursorPos = 4;
				break;
			case 'x':
				cursorPos = 5;
				break;
			case '\\':
				cursorPos = 6;
				break;
			case '\n':
				cursorPos = 7;
				break;
			}

		// Print the list of possible buttons pressed.
		clear();
		mvprintw(0, 2, "UP");
		mvprintw(1, 2, "DOWN");
		mvprintw(2, 2, "LEFT");
		mvprintw(3, 2, "RIGHT");
		mvprintw(4, 2, "A");
		mvprintw(5, 2, "B");
		mvprintw(6, 2, "SELECT");
		mvprintw(7, 2, "START");

		// Print our cursor on the same line as the coresponding button press.
		mvprintw(cursorPos, 0, ">");
		refresh();
		}
	endwin();
	}
