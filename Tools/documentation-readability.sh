#/bin/bash

#     This file is part of LibreWands.

#    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

# ---

# Calculates the readability of our documentation.
# A score of [60-70] is preferred. 

Words=$(cat ./Documentation/Articles/* | grep -vE "^[\{}]" | wc -w)
Sents=$(cat ./Documentation/Articles/* | tr . "\\n" | grep -vE "^[\{}]" | grep -vE "$^" | grep -vE "^ $" | wc -l)
Sylls=$(cat ./Documentation/Articles/* | grep -vE "^[\{}]" | tr ' ' "\\n" | grep -vE "e$" | grep -ioE "[aeiouy]" | wc -l)

AWPS=$(echo "($Words / $Sents)" | bc -l)
AWPY=$(echo "($Sylls / $Words)" | bc -l)
Flsh=$(echo "206.835 - (1.015 * $AWPS) - (84.6 * $AWPY)" | bc -l)

echo "$Words words in documentation per $Sents sentences. ($AWPS)"
echo "$Sylls syllables in documentation per $Words words. ($AWPY)"
echo "Readability score: $Flsh"

