//     This file is part of LibreWands.

//    LibreWands is free software: you can redistribute it and/or modify it under the terms of the
//    GNU General Public License as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.

//    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
//    the GNU General Public License for more details.

//   You should have received a copy of the GNU General Public License along with LibreWands. If
//   not, see https://www.gnu.org/licenses/.

#include "../Source/engineCore.c"
#include <stdio.h>
#include <stdlib.h>

struct mapmonster mon[5];

// The test function
int test(int input, int value)
	{
	// If our input doesn't match the expected value
	if (input != value)
		{
		// The test failed.
		printf("Test failed.\n");
		exit(1);
		}
	else
		{
		// Unless it passed.
		printf("Passed. \n");
		return 0;
		}
	}

// Test our source code for unexpected output.
int main()
	{
	// Test allocating our map memory.
	printf("newMaps(): ");
	test(newMaps(), 0);

	// Test creating base tiles.
	for (int C = 0; C < 512; C++)
		{
		printf("addBase(): ");
		test(addBase(1, 1, 1, 1, 1, 1, 1, 1), 0);
		}

	// Test creating info tiles.
	for (int C = 0; C < 512; C++)
		{
		printf("addInfo(): ");
		test(addInfo(1, 1, 1, 1, 1, 1, 1, "1"), 0);
		}

	// Test creating item tiles.
	for (int C = 0; C < 512; C++)
		{
		printf("addItem(): ");
		test(addItem(1, 1, 1, 1, 1, 1, 1, 1), 0);
		}

	// Test creating door tiles.
	for (int C = 0; C < 512; C++)
		{
		printf("addDoor(): ");
		test(addDoor(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1), 0);
		}

	// Test creating char tiles.
	for (int C = 0; C < 512; C++)
		{
		printf("addChar(): ");
		test(addChar(1, 1, 1, 1, 1, 1, 1, 1, 1, "1", "1", "1", mon), 0);
		}

	// Test deleting tiles.
	printf("delTile(): ");
	test(delTile(1, 1, 1, 1, 1), 0);

	// Test saving maps
	printf("saveMaps(): ");
	test(saveMaps("testfile"), 0);

	// Test saving maps
	printf("loadMaps(): ");
	test(loadMaps("testfile"), 0);
	}
