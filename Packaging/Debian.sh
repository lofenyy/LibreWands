#     This file is part of LibreWands.

#    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

#    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

version=0.0.3

if [ "$1" == "prep" ]
then
cd ..
make clean
make dist
cd Build/
cp "librewands-0.0.3.tar.gz" "librewands_$version.orig.tar.gz"
tar xf "librewands_$version.orig.tar.gz"
cd  "librewands-$version"
mkdir Build/
echo "Trash" > Build/trash
mkdir debian
export EMAIL="lofenyy@linuxmail.org"
dch --create -v "$version-1" --package "librewands"
cd debian
echo "10" > compat
cp ../../../Packaging/Debian_Control control
echo "" > copyright
echo '#!/usr/bin/make -f' > rules
echo "%:" >> rules
printf "\tdh \$@" >> rules
mkdir source/
echo "3.0 (quilt)" > source/format
fi

if [ "$1" == "build" ]
then
cd "../Build/librewands-$version"
debuild -us -uc
fi

