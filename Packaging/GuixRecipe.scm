;    This file is part of LibreWands.

;    LibreWands is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;    LibreWands is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

;   You should have received a copy of the GNU General Public License along with LibreWands. If not, see https://www.gnu.org/licenses/. 

(use-modules (guix packages)
             (guix download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages ncurses)
             (guix licenses)
)

(package
    (name "librewands")
    (version "0.0.3-r1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://gitea.com/lofenyy/LibreWands/archive/" version
                                  ".tar.gz"))
              (sha256
               (base32
                "0l007j42ffpv3inmvi4897lxj2cx4jglgzniihjb9sk1di9mag0d"))))
    (build-system gnu-build-system)
    (inputs `(("ncurses" ,ncurses)))
    (synopsis "An ncurses RPG featuring magick and monsters.")
    (description "An ncurses RPG featuring magick and monsters.")
    (home-page "https://gitea.com/lofenyy/LibreWands")
    (license gpl3+)
)

